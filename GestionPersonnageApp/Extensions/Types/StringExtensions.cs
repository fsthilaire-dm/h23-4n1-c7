﻿namespace GestionPersonnageApp.Extensions.Types;

/// <summary>
/// Classe qui contient les extensions pour les données de type string
/// </summary>
public static class StringExtensions
{
    /// <summary>
    /// Permet de tronquer une string
    /// </summary>
    /// <param name="s">string à tronquer</param>
    /// <param name="maxLongueur">Longueur maximum de la chaine</param>
    /// <returns>string tronqué</returns>
    public static string? Tronquer(this string? s, int maxLongueur)
    {
        //Verifie si la chaine est plus grande que la longueur maximale
        if (s?.Length > maxLongueur)
        {
            //La chaine est plus grande que la longueur maximamle
            //Retourne uniquement la partie de la longueur maximale
            return s.Substring(0, maxLongueur);
        }
        else
        {
            //La chaine est plus petite que la longueur maximale
            //Retourne la chaine originale
            return s;
        }
    }
}
