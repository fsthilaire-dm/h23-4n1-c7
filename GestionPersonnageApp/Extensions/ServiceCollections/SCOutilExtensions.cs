﻿namespace GestionPersonnageApp.Extensions.ServiceCollections;

/// <summary>
/// Classe d'extension qui permet d'enregistrer les classes de la catégorie Genetateur
/// </summary>
public static class SCOutilExtensions
{
    /// <summary>
    /// Méthode qui permet d'enregistrer les outils de l'application
    /// </summary>
    /// <param name="services">La collection de services</param>
    public static void EnregistrerOutils(this IServiceCollection services)
    {
        services.AddTransient<IUniversGenerateur, UniversGenerateur>();
        services.AddTransient<IPersonnageGenerateur, PersonnageGenerateur>();
        services.AddTransient<IFilmGenerateur, FilmGenerateur>();
        services.AddTransient<IDistributionGenerateur, DistributionGenerateur>();        
    }
}