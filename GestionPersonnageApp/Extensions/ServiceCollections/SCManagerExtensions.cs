﻿using Microsoft.Extensions.DependencyInjection;

namespace GestionPersonnageApp.Extensions.ServiceCollections;

/// <summary>
/// Classe d'extension qui permet d'enregistrer les classes de la catégorie Manager
/// </summary>
public static class SCManagerExtensions
{
    /// <summary>
    /// Méthode qui permet d'enregistrer les managers de l'application
    /// </summary>
    /// <param name="services">La collection de services</param>
    public static void EnregistrerManagers(this IServiceCollection services)
    {
        services.AddTransient<IUniversManager, UniversManager>();
        services.AddTransient<IFilmManager, FilmManager>();
        services.AddTransient<IPersonnageManager, PersonnageManager>();
        services.AddTransient<IDistributionManager, DistributionManager>();
    }
}
