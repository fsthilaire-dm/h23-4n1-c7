﻿namespace GestionPersonnageApp.Extensions.Data;

/// <summary>
/// Classe statique qui regroupe les méthodes d'extension pour la console du modèle Univers
/// </summary>
public static class UniversConsoleExtensions
{
    /// <summary>
    /// Méthode qui affiche l'information d'un univers à la console
    /// </summary>
    /// <param name="univers">Univers</param>
    public static void AfficherConsole(this Univers? univers)
    {
        if (univers != null)
        {
            Console.WriteLine($"Id : {univers.UniversId}");
            Console.WriteLine($"Nom : {univers.Nom}");
            Console.WriteLine($"Année de création : {univers.AnneeCreation}");
            Console.WriteLine($"Site Web : {univers.SiteWeb}");
            Console.WriteLine($"Propriétaire : {univers.Proprietaire}");
        }
        else
        {
            Console.WriteLine("Univers non trouvé.");
        }
    }

    /// <summary>
    /// Méthode qui affiche l'information d'une liste d'univers à la console
    /// </summary>
    /// <param name="lstUnivers"></param>
    public static void AfficherConsole(this List<Univers> lstUnivers)
    {
        if (lstUnivers?.Count > 0)
        {
            foreach (Univers univers in lstUnivers)
            {
                univers.AfficherConsole();
                Console.WriteLine(Environment.NewLine);
            }
        }
        else
        {
            Console.WriteLine("Il n'y a aucun univers dans la base de données.");
        }
    }

    /// <summary>
    /// Méthode qui demande à l'utilisateur de supprimer l'univers. 
    /// Fait la validation utilisateur avant d'offrir la suppression.
    /// </summary>
    /// <param name="universDependance">L'univers et ses dépendances</param>
    /// <returns>true si doit être supprimé, false si ne peut pas ou ne doit pas être supprimé</returns>
    public static bool SupprimerConsole(this UniversDependance? universDependance)
    {
        if(universDependance != null)
        {
            universDependance.Univers.AfficherConsole();

            //Vérifie si l'univers peut être supprimé
            if(universDependance.NbPersonnages == 0)
            {
                //L'univers peut être supprimé

                /*Code à remplacer pour utiliser une classe utilitaire*/
                //Mettre ce genre de questions dans un utilitaire.
                //Il faut s'assurer que c'est seulement O ou N. 
                //Dans cet exemple, le cas Z sera considéré comme un N

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Voulez-vous supprimer cet univers ? [O/N]");
                Console.ForegroundColor = ConsoleColor.Gray;

                if (Console.ReadLine()?.ToUpper() == "O")
                {
                    return true;
                }
                else
                {
                    return false;
                }

                /*Fin du code à remplacer*/
            }
            else
            {
                //L'univers ne peut pas être supprimé
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Impossible de supprimer cet univers. Il y a encore des dépandences pour cet univers.");
                Console.WriteLine($"Nombre de personnages : {universDependance.NbPersonnages}");
                Console.ForegroundColor = ConsoleColor.Gray;

                return false;
            }
        }
        else
        {
            Console.WriteLine("Univers non trouvé.");
            return false;
        }
    }
}
