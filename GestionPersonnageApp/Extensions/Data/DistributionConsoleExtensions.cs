﻿namespace GestionPersonnageApp.Extensions.Data;

/// <summary>
/// Classe statique qui regroupe les méthodes d'extension pour la console du modèle Distribution
/// </summary>
public static class DistributionConsoleExtensions
{
    private const string PAS_TROUVE_PAR_CLE = "Distribution non trouvée.";

    /// <summary>
    /// Méthode qui affiche l'information d'une distribution à la console
    /// </summary>
    /// <param name="distribution">Distribution</param>
    public static void AfficherConsole(this Distribution? distribution)
    {
        if (distribution != null)
        {
            Console.WriteLine($"Personnage Id : {distribution.PersonnageId}");

            if(distribution.Personnage != null)
            {
                Console.WriteLine($"Personnage : {distribution.Personnage.Nom}");
            }

            Console.WriteLine($"Film Id : {distribution.FilmId}");

            if (distribution.Film != null)
            {
                Console.WriteLine($"Film : {distribution.Film.Titre}");
            }

            Console.WriteLine($"Acteur : {distribution.Acteur}");
        }
        else
        {
            Console.WriteLine(PAS_TROUVE_PAR_CLE);
        }
    }

    /// <summary>
    /// Méthode qui affiche l'information d'une liste de distribution à la console
    /// </summary>
    /// <param name="lstFilm"></param>
    public static void AfficherConsole(this List<Distribution> lstDistribution)
    {
        if (lstDistribution?.Count > 0)
        {
            foreach (Distribution distribution in lstDistribution)
            {
                distribution.AfficherConsole();
                Console.WriteLine(Environment.NewLine);
            }
        }
        else
        {
            Console.WriteLine("Il n'y a aucune distribution dans la base de données.");
        }
    }

    /// <summary>
    /// Méthode qui demande à l'utilisateur de supprimer la distribution.     
    /// </summary>
    /// <param name="distribution">La distribution</param>
    /// <returns>true si la distribution doit être supprimée, false si elle ne doit pas être supprimée</returns>
    public static bool SupprimerConsole(this Distribution? distribution)
    {
        if (distribution != null)
        {
            distribution.AfficherConsole();

            /*Code à remplacer pour utiliser une classe utilitaire*/
            //Mettre ce genre de questions dans un utilitaire.
            //Il faut s'assurer que c'est seulement O ou N. 
            //Dans cet exemple, le cas Z sera considéré comme un N

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Voulez-vous supprimer cette distribution ? [O/N]");
            Console.ForegroundColor = ConsoleColor.Gray;

            if (Console.ReadLine()?.ToUpper() == "O")
            {
                return true;
            }
            else
            {
                return false;
            }

            /*Fin du code à remplacer*/
        }
        else
        {
            Console.WriteLine(PAS_TROUVE_PAR_CLE);
            return false;
        }
    }
}
