﻿namespace GestionPersonnageApp.Services;

/// <summary>
/// Interface qui contient les services du modèle Personnage
/// </summary>
public interface IPersonnageService
{
    /// <summary>
    /// Obtenir la liste des personnages
    /// </summary>
    /// <returns>La liste des personnages</returns>
    List<Personnage> ObtenirListe();

    /// <summary>
    /// Obtenir la liste des personnages
    /// </summary>
    /// <param name="inclureUnivers">Inclure l'univers ou non dans le modèle</param>
    /// <returns>La liste des personnages</returns>
    List<Personnage> ObtenirListe(bool inclureUnivers);

    /// <summary>
    /// Obtenir un personnage en fonction de sa clé primaire
    /// </summary>
    /// <param name="personnageId">Clé primaire du personnage</param>
    /// <returns>Le personnage correspondant à la clé primaire ou null si non trouvé</returns>
    Personnage? ObtenirPersonnage(int personnageId);

    /// <summary>
    /// Obtenir un personnage en fonction de sa clé primaire
    /// </summary>
    /// <param name="personnageId">Clé primaire du personnage</param>
    /// <param name="inclureUnivers">Inclure l'univers ou non dans le modèle</param>
    /// <returns>Le personnage correspondant à la clé primaire ou null si non trouvé</returns>
    Personnage? ObtenirPersonnage(int personnageId, bool inclureUnivers);

    /// <summary>
    /// Obtenir le rapport des statisques des films pour les personnages d'un univers précis
    /// </summary>
    /// <param name="universId">La clé de l'univers des personnages</param>
    /// <param name="utiliserVue">Utiliser la Vue SQL ou en pur LINQ</param>
    /// <returns>Liste des statistiques par personnage</returns>
    List<PersonnageStatFilms> ObtenirRapportPersonnageStatFilms(int universId, bool utiliserVue);
}