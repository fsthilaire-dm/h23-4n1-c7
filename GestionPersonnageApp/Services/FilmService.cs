﻿namespace GestionPersonnageApp.Services;

/// <summary>
/// Classe qui contient les services du modèle Film
/// </summary>
public class FilmService : IFilmService
{
    private readonly IFilmRepo _filmRepo;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="filmRepo">Le repository Film</param>
    public FilmService(IFilmRepo filmRepo)
    {
        _filmRepo = filmRepo;
    }

    public Film? ObtenirFilm(int filmId)
    {
        return _filmRepo.ObtenirFilm(filmId);
    }

    public List<Film> ObtenirListe()
    {
        return _filmRepo.ObtenirListe();
    }

    public Film? ObtenirFilmDetail(int filmId)
    {
        return _filmRepo.ObtenirFilmDetail(filmId);
    }
}
