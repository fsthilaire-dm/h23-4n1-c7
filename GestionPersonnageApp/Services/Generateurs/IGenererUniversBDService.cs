﻿namespace GestionPersonnageApp.Services.Generateurs;

/// <summary>
/// Interface qui contient le service de génération de données dans la base de données pour la table Univers
/// </summary>
public interface IGenererUniversBDService
{
    /// <summary>
    /// Générer des données dans la base de données
    /// </summary>
    /// <param name="quantite">Quantité à générer</param>
    void Generer(int quantite);
}