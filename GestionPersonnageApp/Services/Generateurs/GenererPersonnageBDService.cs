﻿namespace GestionPersonnageApp.Services.Generateurs;

/// <summary>
/// Classe qui contient le service de génération de données dans la base de données pour la table Univers
/// </summary>
public class GenererPersonnageBDService : IGenererPersonnageBDService
{
    private readonly IUniversRepo _universRepo;
    private readonly IPersonnageRepo _personnageRepo;
    private readonly IPersonnageGenerateur _personnageGenerateur;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="universRepo">Le repository Univers</param>
    /// <param name="personnageRepo">Le repository Personnage</param>
    /// <param name="personnageGenerateur">Le générateur de personnages</param> 
    public GenererPersonnageBDService(IUniversRepo universRepo, IPersonnageRepo personnageRepo,
        IPersonnageGenerateur personnageGenerateur)
    {
        _universRepo = universRepo;
        _personnageRepo = personnageRepo;
        _personnageGenerateur = personnageGenerateur;
    }

    public void Generer(int quantite)
    {
        if (quantite > 0)
        {
            //Assigne la liste des univers id dans le générateur de personnages
            _personnageGenerateur.AssignerListeUniversId(_universRepo.ObtenirListeUniversId());

            int nbGenere = 0;
            int nbException = 0;

            while (nbGenere < quantite)
            {
                Personnage personnage = _personnageGenerateur.Generer();

                try
                {
                    _personnageRepo.AjouterPersonnage(personnage);

                    nbGenere++;
                    nbException = 0;
                }
                catch (DbUpdateException)
                {
                    //La base de données n'a pas accepté le personnage
                    //Il faut générer de nouveau
                    //Les autres types d'exceptions feront planter le programme
                    nbException++;

                    //Si le nombre d'exceptions atteint la limite, il faut arrêter le programme
                    if (nbException > 100)
                    {
                        //La limite est atteinte
                        //Soulève l'exception
                        throw;
                    }
                }
            }
        }
        else
        {
            throw new Exception("La quantité à générer doit être plus grande que zéro.");
        }
    }
}