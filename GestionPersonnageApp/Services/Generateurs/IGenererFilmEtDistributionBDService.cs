﻿namespace GestionPersonnageApp.Services.Generateurs;

/// <summary>
/// Interface qui contient le service de génération de données dans la base de données pour les tables Film et Personnage
/// </summary>
public interface IGenererFilmEtDistributionBDService
{
    /// <summary>
    /// Générer des données dans la base de données
    /// </summary>
    /// <param name="quantite">Quantité de film à générer</param>
    void Generer(int quantite);
}