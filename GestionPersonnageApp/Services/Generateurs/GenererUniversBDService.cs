﻿namespace GestionPersonnageApp.Services.Generateurs;

/// <summary>
/// Classe qui contient le service de génération de données dans la base de données pour la table Univers
/// </summary>
public class GenererUniversBDService : IGenererUniversBDService
{
    private readonly IUniversRepo _universRepo;
    private readonly IUniversGenerateur _universGenerateur;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="universRepo">Le repository Univers</param>
    /// <param name="universGenerateur">Le générateur d'univers</param>
    public GenererUniversBDService(IUniversRepo universRepo, IUniversGenerateur universGenerateur)
    {
        _universRepo = universRepo;
        _universGenerateur = universGenerateur;
    }

    public void Generer(int quantite)
    {
        if (quantite > 0)
        {
            int nbGenere = 0;
            int nbException = 0;

            while (nbGenere < quantite)
            {
                Univers univers = _universGenerateur.Generer();
                
                try
                {
                    _universRepo.AjouterUnivers(univers);

                    nbGenere++;
                    nbException = 0;
                }
                catch (DbUpdateException)
                {
                    //La base de données n'a pas accepté l'univers
                    //Il faut générer de nouveau
                    //Les autres types d'exceptions feront planter le programme
                    nbException++;

                    //Si le nombre d'exceptions atteint la limite, il faut arrêter le programme
                    if(nbException > 100)
                    {
                        //La limite est atteinte
                        //Soulève l'exception
                        throw;
                    }
                }
            }
        }
        else
        {
            throw new Exception("La quantité à générer doit être plus grande que zéro.");
        }
    }
}
