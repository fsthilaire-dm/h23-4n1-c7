﻿namespace GestionPersonnageApp.Services.Generateurs;

/// <summary>
/// Interface qui contient le service de génération de données dans la base de données pour la table Personnage
/// </summary>
public interface IGenererPersonnageBDService
{
    /// <summary>
    /// Générer des données dans la base de données
    /// </summary>
    /// <param name="quantite">Quantité à générer</param>
    void Generer(int quantite);
}