﻿namespace GestionPersonnageApp.Services.Generateurs;

/// <summary>
/// Classe qui contient le service de génération de données dans la base de données pour les tables Film et Distribution
/// </summary>
public class GenererFilmEtDistributionBDService : IGenererFilmEtDistributionBDService
{
    private readonly IPersonnageRepo _personnageRepo;
    private readonly IFilmRepo _filmRepo;
    private readonly IFilmGenerateur _filmGenerateur;
    private readonly IDistributionGenerateur _distributionGenerateur;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="personnageRepo">Le repository Personnage</param>
    /// <param name="filmRepo">Le repository Film</param>
    /// <param name="filmGenerateur">Le générateur de films</param>
    /// <param name="distributionGenerateur">Le générateur de distributions</param>
    public GenererFilmEtDistributionBDService(IPersonnageRepo personnageRepo, IFilmRepo filmRepo,
        IFilmGenerateur filmGenerateur, IDistributionGenerateur distributionGenerateur)
    {
        _personnageRepo = personnageRepo;
        _filmRepo = filmRepo;
        _filmGenerateur = filmGenerateur;
        _distributionGenerateur = distributionGenerateur;
    }

    public void Generer(int quantite)
    {
        if (quantite > 0)
        {
            //Assigne la liste des PersonnageId de la base de données dans le générateur de distributions
            _distributionGenerateur.AssignerListePersonnageId(_personnageRepo.ObtenirListePersonnageId());

            int nbGenere = 0;
            int nbException = 0;

            while (nbGenere < quantite)
            {
                //Génère le film
                Film film = _filmGenerateur.Generer();

                //Génère entre 1 et 10 distributions pour ce film
                _distributionGenerateur.Generer(film, 1, 10);

                try
                {
                    //Ajoute le film dans la base de données
                    //Le film contient la liste des distributions.
                    //Les distributions vont s'ajouter en même temps.
                    _filmRepo.AjouterFilm(film);

                    nbGenere++;
                    nbException = 0;
                }
                catch (DbUpdateException)
                {
                    //La base de données n'a pas accepté le film et/ou les distributions
                    //Il faut générer de nouveau
                    //Les autres types d'exceptions feront planter le programme
                    nbException++;

                    //Si le nombre d'exceptions atteint la limite, il faut arrêter le programme
                    if (nbException > 100)
                    {
                        //La limite est atteinte
                        //Soulève l'exception
                        throw;
                    }
                }
            }
        }
        else
        {
            throw new Exception("La quantité à générer doit être plus grande que zéro.");
        }
    }
}
