﻿namespace GestionPersonnageApp.Services;

/// <summary>
/// Classe qui contient les services du modèle Distribution
/// </summary>
public class DistributionService : IDistributionService
{
    private readonly IDistributionRepo _distributionRepo;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="distributionRepo">Le repository Repo</param>
    public DistributionService(IDistributionRepo distributionRepo)
    {
        _distributionRepo = distributionRepo;
    }   

    public Distribution? ObtenirDistribution(int personnageId, int filmId)
    {
        return _distributionRepo.ObtenirDistribution(personnageId, filmId);
    }

    public Distribution? ObtenirDistribution(int personnageId, int filmId, bool inclurePersonnageEtFilm)
    {
        return _distributionRepo.ObtenirDistribution(personnageId, filmId, inclurePersonnageEtFilm);
    }

    public List<Distribution> ObtenirListe()
    {
        return _distributionRepo.ObtenirListe();
    }

    public List<Distribution> ObtenirListe(bool inclurePersonnageEtFilm)
    {
        return _distributionRepo.ObtenirListe(inclurePersonnageEtFilm);
    }

    public void SupprimerDistribution(int personnageId, int filmId)
    {
        Distribution? distribution = _distributionRepo.ObtenirDistribution(personnageId, filmId);

        if (distribution != null)
        {
            _distributionRepo.SupprimerDistribution(distribution);
        }
        else
        {
            throw new Exception("La distribution n'existe pas dans la base de données.");
        }
    }
        
    public bool ModifierActeur(int personnageId, int filmId, string acteur)
    {
        //Vérifie si la valeur du champ acteur correspond aux contraintes de la base de données
        if (acteur?.Length > 0 && acteur?.Length <= 100)
        {
            //Le champ acteur correspond aux contraintes de la base de données
            Distribution? distribution = ObtenirDistribution(personnageId, filmId);

            //Vérifie si la distribution est trouvée
            if (distribution != null)
            {
                //La distribution est trouvée
                distribution.Acteur = acteur;

                _distributionRepo.EnregistrerContexte();

                return true;
                
            }
            else
            {
                //La distribution n'est pas trouvée
                //Impossible de mettre à jour
                throw new Exception("La distribution n'existe pas. Impossible de mettre à jour.");
            }
        }
        else
        {
            //La longueur du champ ne recptecte pas les contraintes de la base de données
            return false;
        }
    }
}
