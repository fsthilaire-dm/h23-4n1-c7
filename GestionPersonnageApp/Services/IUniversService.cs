﻿namespace GestionPersonnageApp.Services;

/// <summary>
/// Interface qui contient les services du modèle Univers
/// </summary>
public interface IUniversService
{
    /// <summary>
    /// Obtenir la liste des univers
    /// </summary>
    /// <returns>La liste des univers</returns>
    List<Univers> ObtenirListe();

    /// <summary>
    /// Obtenir un univers en fonction de sa clé primaire
    /// </summary>
    /// <param name="universId">Clé primaire de l'univers</param>
    /// <returns>L'univers correspondant à la clé primaire ou null si non trouvé</returns>
    Univers? ObtenirUnivers(int universId);

    /// <summary>
    /// Obtenir les dépendances pour un univers précis
    /// </summary>
    /// <param name="universId">Clé primaire</param>
    /// <returns>Les dépendances de l'univers s'il existe, sinon null si non trouvé</returns>
    UniversDependance? ObtenirDependance(int universId);

    /// <summary>
    /// Supprimer l'univers de la base de données
    /// </summary>
    /// <param name="universId">Clé primaire à supprimer</param>
    void SupprimerUnivers(int universId);
}