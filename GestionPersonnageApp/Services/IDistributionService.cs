﻿namespace GestionPersonnageApp.Services;

/// <summary>
/// Interface qui contient les services du modèle Distribution
/// </summary>
public interface IDistributionService
{
    /// <summary>
    /// Obtenir une distribution à partir de sa clé primaire
    /// </summary>
    /// <param name="personnageId">Clé primaire du personnage</param>
    /// <param name="filmId">Clé primaire du film</param>
    /// <returns>Le distribution s'il existe, sinon null si non trouvé</returns>    
    Distribution? ObtenirDistribution(int personnageId, int filmId);

    /// <summary>
    /// Obtenir une distribution à partir de sa clé primaire
    /// </summary>
    /// <param name="personnageId">Clé primaire du personnage</param>
    /// <param name="filmId">Clé primaire du film</param>
    /// <param name="inclurePersonnageEtFilm">Inclure les relations Personnage et Film ou non dans le modèle</param>
    /// <returns>Le distribution s'il existe, sinon null si non trouvé</returns>
    Distribution? ObtenirDistribution(int personnageId, int filmId, bool inclurePersonnageEtFilm);

    /// <summary>
    /// Obtenir la liste de toutes les distributions
    /// </summary>
    /// <returns>Liste des distributions de la base de données</returns>
    List<Distribution> ObtenirListe();

    /// <summary>
    /// Obtenir la liste de toutes les distributions
    /// </summary>
    /// <param name="inclurePersonnageEtFilm">Inclure les relations Personnage et Film ou non dans le modèle</param>
    /// <returns>Liste des distributions de la base de données</returns>
    List<Distribution> ObtenirListe(bool inclurePersonnageEtFilm);

    /// <summary>
    /// Supprimer la distribution de la base de données
    /// </summary>
    /// <param name="personnageId">Clé primaire du personnage</param>
    /// <param name="filmId">Clé primaire du film</param>
    void SupprimerDistribution(int personnageId, int filmId);


    /// <summary>
    /// Modifier l'acteur pour une distribution
    /// </summary>
    /// <param name="personnageId">Clé primaire du personnage</param>
    /// <param name="filmId">Clé primaire du film</param>
    /// <param name="acteur">Le nouvel acteur</param>
    /// <returns></returns>
    bool ModifierActeur(int personnageId, int filmId, string acteur);
}
