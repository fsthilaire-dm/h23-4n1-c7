﻿         
namespace GestionPersonnageApp.Repositories;

/// <summary>
/// Classe qui contient les méthodes de communication avec la base de données pour la table Distribution
/// </summary>
public class DistributionRepo : IDistributionRepo
{
    private readonly GestionPersonnageContext _db;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="db">Contexte de la base de données GestionPersonnage</param>
    public DistributionRepo(GestionPersonnageContext db)
    {
        _db = db;
    }

    public List<Distribution> ObtenirListe()
    {
        return _db.Distribution.ToList();
    }

    public List<Distribution> ObtenirListe(bool inclurePersonnageEtFilm)
    {
        if (inclurePersonnageEtFilm == true)
        {

            return _db.Distribution
                .Include(d => d.Personnage)
                .Include(d => d.Film)
                .ToList();
        }
        else
        {
            return ObtenirListe();
        }
    }

    public Distribution? ObtenirDistribution(int personnageId, int filmId)
    {
        return _db.Distribution.Where(d => d.PersonnageId == personnageId && d.FilmId == filmId).FirstOrDefault();
    }

    public Distribution? ObtenirDistribution(int personnageId, int filmId, bool inclurePersonnageEtFilm)
    {
        if (inclurePersonnageEtFilm == true)
        {
            return (from lqDistribution in _db.Distribution
                        .Include(d => d.Personnage)
                        .Include(d => d.Film)
                    where
                        lqDistribution.PersonnageId == personnageId &&
                        lqDistribution.FilmId == filmId
                    select
                        lqDistribution).FirstOrDefault();
        }
        else
        {
            return ObtenirDistribution(personnageId, filmId);
        }
    }

    public void SupprimerDistribution(Distribution distribution)
    {
        _db.Remove(distribution);

        _db.SaveChanges();
    }

    public void EnregistrerContexte()
    {
        _db.SaveChanges();
    }
}