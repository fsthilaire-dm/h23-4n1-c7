﻿namespace GestionPersonnageApp.Repositories;

/// <summary>
/// Interface qui contient les méthodes de communication avec la base de données pour la table Univers
/// </summary>
public interface IUniversRepo
{
    /// <summary>
    /// Obtenir un univers à partir de sa clé primaire
    /// </summary>
    /// <param name="universId">Clé primaire</param>
    /// <returns>L'univers s'il existe, sinon null si non trouvé</returns>
    Univers? ObtenirUnivers(int universId);

    /// <summary>
    /// Obtenir la liste de tous les univers
    /// </summary>
    /// <returns>Liste des univers de la base de données</returns>
    List<Univers> ObtenirListe();

    /// <summary>
    /// Obtenir les dépendances pour un univers précis
    /// </summary>
    /// <param name="universId">Clé primaire</param>
    /// <returns>Les dépenances de l'univers s'il existe, sinon null si non trouvé</returns>
    UniversDependance? ObtenirDependance(int universId);

    /// <summary>
    /// Supprimer l'univers de la base de données
    /// </summary>
    /// <param name="univers">L'univers à supprimer</param>
    void SupprimerUnivers(Univers univers);

    /// <summary>
    /// Ajouter l'univers dans la base de données
    /// </summary>
    /// <param name="univers">L'univers à ajouter</param>
    void AjouterUnivers(Univers univers);

    /// <summary>
    /// Obtenir la liste de tous les universId
    /// </summary>
    /// <returns>La liste de tous les universId</returns>
    List<int> ObtenirListeUniversId();
}