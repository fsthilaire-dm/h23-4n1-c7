﻿namespace GestionPersonnageApp.Repositories;

/// <summary>
/// Classe qui contient les méthodes de communication avec la base de données pour la table Film
/// </summary>
public class FilmRepo : IFilmRepo
{
    private readonly GestionPersonnageContext _db;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="db">Contexte de la base de données GestionPersonnage</param>
    public FilmRepo(GestionPersonnageContext db)
    {
        _db = db;
    }

    public List<Film> ObtenirListe()
    {
        return _db.Film.ToList();
    }

    public Film? ObtenirFilm(int filmId)
    {
        return _db.Film.Where(f => f.FilmId == filmId).FirstOrDefault();
    }

    public Film? ObtenirFilmDetail(int filmId)
    {
        return (from lqFilm in _db.Film
                    .Include(f => f.Distribution)
                        .ThenInclude(d => d.Personnage)
                where
                    lqFilm.FilmId == filmId
                select
                    lqFilm).FirstOrDefault();
    }
        
    public void AjouterFilm(Film film)
    {
        _db.Add(film);

        _db.SaveChanges();
    }
}
