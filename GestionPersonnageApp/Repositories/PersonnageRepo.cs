﻿using GestionPersonnageApp.Data;

namespace GestionPersonnageApp.Repositories;

/// <summary>
/// Classe qui contient les méthodes de communication avec la base de données pour la table Personnage
/// </summary>
public class PersonnageRepo : IPersonnageRepo
{
    private readonly GestionPersonnageContext _db;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="db">Contexte de la base de données GestionPersonnage</param>
    public PersonnageRepo(GestionPersonnageContext db)
    {
        _db = db;
    }

    public List<Personnage> ObtenirListe()
    {
        return _db.Personnage.ToList();
    }

    public List<Personnage> ObtenirListe(bool inclureUnivers)
    {
        if (inclureUnivers == true)
        {
            return _db.Personnage.Include(p => p.Univers).ToList();
        }
        else
        {
            return ObtenirListe();
        }
    }

    public Personnage? ObtenirPersonnage(int personnageId)
    {
        return _db.Personnage.Where(p => p.PersonnageId == personnageId).FirstOrDefault();
    }

    public Personnage? ObtenirPersonnage(int personnageId, bool inclureUnivers)
    {
        if (inclureUnivers == true)
        {
            return (from lqPersonnage in _db.Personnage
                        .Include(p => p.Univers)
                    where
                        lqPersonnage.PersonnageId == personnageId
                    select
                        lqPersonnage).FirstOrDefault();
        }
        else
        {
            return ObtenirPersonnage(personnageId);
        }
    }

    public List<PersonnageStatFilms> ObtenirRapportPersonnageStatFilms(int universId, bool utiliserVue)
    {
        if (utiliserVue == true)
        {
            return (from lqVue in _db.VRapportPersonnageStatFilms
                    where
                        lqVue.UniversId == universId
                    orderby
                        lqVue.PersonnageNom
                    select
                        new PersonnageStatFilms()
                        {
                            PersonnageId = lqVue.PersonnageId,
                            PersonnageNom = lqVue.PersonnageNom,
                            UniversId = lqVue.UniversId,
                            UniversNom = lqVue.UniversNom,
                            BudgetMoyen = lqVue.BudgetMoyen.GetValueOrDefault(),
                            NbFilm = lqVue.NbFilm.GetValueOrDefault(),
                            DureeTotale = lqVue.DureeTotale.GetValueOrDefault(),
                        }).ToList();
        }
        else
        {
            return (from lqPersonnage in _db.Personnage
                    where
                        lqPersonnage.UniversId == universId
                    orderby
                        lqPersonnage.Nom
                    select
                       new PersonnageStatFilms()
                       {
                           PersonnageId = lqPersonnage.PersonnageId,
                           PersonnageNom = lqPersonnage.Nom,
                           UniversId = lqPersonnage.UniversId,
                           UniversNom = lqPersonnage.Univers.Nom,
                           BudgetMoyen = lqPersonnage.Distribution.Average(d => d.Film.Budget),
                           NbFilm = lqPersonnage.Distribution.Count(),
                           DureeTotale = lqPersonnage.Distribution.Sum(d => d.Film.Duree)
                       }).ToList();
        }
    }

    public void AjouterPersonnage(Personnage personnage)
    {
        _db.Add(personnage);

        _db.SaveChanges();
    }
    
    public List<int> ObtenirListePersonnageId()
    {
        return _db.Personnage.Select(p => p.PersonnageId).ToList();
    }
}