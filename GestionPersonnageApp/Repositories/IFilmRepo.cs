﻿namespace GestionPersonnageApp.Repositories;

/// <summary>
/// Interface qui contient les méthodes de communication avec la base de données pour la table Film
/// </summary>
public interface IFilmRepo
{
    /// <summary>
    /// Obtenir un film à partir de sa clé primaire
    /// </summary>
    /// <param name="FilmId">Clé primaire</param>
    /// <returns>Le film s'il existe, sinon null si non trouvé</returns>
    Film? ObtenirFilm(int FilmId);

    /// <summary>
    /// Obtenir la liste de tous les films
    /// </summary>
    /// <returns>Liste des films de la base de données</returns>
    List<Film> ObtenirListe();

    /// <summary>
    /// Obtenir le détail du film (Inclure la table Distribution et Personnage)
    /// </summary>
    /// <param name="filmId"></param>
    /// <param name="FilmId">Clé primaire</param>
    /// <returns>Le film s'il existe, sinon null si non trouvé</returns>
    Film? ObtenirFilmDetail(int filmId);

    /// <summary>
    /// Ajouter le film dans la base de données
    /// </summary>
    /// <param name="film">Le personnage à ajouter</param>
    void AjouterFilm(Film film);
}