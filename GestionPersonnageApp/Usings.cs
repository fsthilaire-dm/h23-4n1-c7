﻿global using GestionPersonnageApp; //Les classes à la racine de l'application
global using GestionPersonnageApp.Data; //Les classes du modèle du contexte
global using GestionPersonnageApp.Data.Rapports; //Les classes de type Rapport
global using GestionPersonnageApp.Data.Dependances; //Les classes de type Dependance
global using GestionPersonnageApp.Data.Context; //La classe du contexte
global using GestionPersonnageApp.Managers; //Les classes de type Manager
global using GestionPersonnageApp.Repositories; //Les classes de type Repository
global using GestionPersonnageApp.Services; //Les classes de type Service
global using GestionPersonnageApp.Extensions.Data; //Extensions des modèles du contexte
global using Microsoft.Extensions.DependencyInjection; //Les classes pour l'injection de dépendances
global using Microsoft.EntityFrameworkCore; //Les classes et les méthodes de Entity Framework
global using GestionPersonnageApp.Extensions.Types; //Extensions pour les types de base
global using Bogus; //Les classes et les méthodes de Bogus
global using GestionPersonnageApp.Outils; //Les classes pour les outils de l'application, dont les générateurs
global using GestionPersonnageApp.Services.Generateurs; //Les services spécialisés de génération dans la BD