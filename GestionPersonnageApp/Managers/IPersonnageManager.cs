﻿namespace GestionPersonnageApp.Managers;

/// <summary>
/// Interface qui s'occupe de la coordination du modèle Personnage
/// </summary>
public interface IPersonnageManager
{
    /// <summary>
    /// Afficher tous les personnages
    /// </summary>
    void AfficherListe();

    /// <summary>
    /// Afficher un personnage en fonction de sa clé primaire
    /// </summary>        
    void AfficherParId();

    /// <summary>
    /// Aficher le rapport des statisques des films pour les personnages d'un univers précis
    /// </summary>    
    void AfficherRapportPersonnageStatFilms();

    /// <summary>
    /// Générer des personnages dans la base de de données
    /// </summary>
    void GenererDonneesBD();
}