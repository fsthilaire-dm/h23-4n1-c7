﻿namespace GestionPersonnageApp.Managers;

/// <summary>
/// Classe qui s'occupe de la coordination de modèle Personnage
/// </summary>
public class PersonnageManager : IPersonnageManager
{
    private readonly IPersonnageService _personnageService;
    private readonly IGenererPersonnageBDService _genererPersonnageBDService;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="personnageService">Service du modèle personnage</param>
    /// <param name="_genererPersonnageBDService">Service de génération de données dans la BD pour du modèle Personnage </param>
    public PersonnageManager(IPersonnageService personnageService, IGenererPersonnageBDService genererPersonnageBDService)
    {
        _personnageService = personnageService;
        _genererPersonnageBDService = genererPersonnageBDService;
    }

    public void AfficherListe()
    {
        /*Code à remplacer pour utiliser une classe utilitaire*/
        //Mettre ce genre de questions dans un utilitaire.
        //Il faut s'assurer que c'est seulement O ou N. 
        //Dans cet exemple, le cas Z sera considéré comme un N

        Console.WriteLine("Il faut afficher le détail des clés étrangères ? [O/N]");
        bool afficherUnivers;

        if (Console.ReadLine()?.ToUpper() == "O")
        {
            afficherUnivers = true;
        }
        else
        {
            afficherUnivers = false;
        }

        /*Fin du code à remplacer*/

        //Utilisation directe de l'affichage
        _personnageService.ObtenirListe(afficherUnivers).AfficherConsole();
    }

    public void AfficherParId()
    {

        /*Code à remplacer pour utiliser une classe utilitaire*/
        int personnageId;
        bool valide;
        do
        {
            Console.WriteLine("Entrer la clé du personnage.");

            valide = Int32.TryParse(Console.ReadLine(), out personnageId);

            if (valide == false)
            {
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
            }

        } while (valide == false);


        //Mettre ce genre de questions dans un utilitaire.
        //Il faut s'assurer que c'est seulement O ou N. 
        //Dans cet exemple, le cas Z sera considéré comme un N

        Console.WriteLine("Il faut afficher le détail des clés étrangères ? [O/N]");
        bool afficherUnivers;

        if (Console.ReadLine()?.ToUpper() == "O")
        {
            afficherUnivers = true;
        }
        else
        {
            afficherUnivers = false;
        }

        /*Fin du code à remplacer*/

        //Utilisation directe de l'affichage
        _personnageService.ObtenirPersonnage(personnageId, afficherUnivers).AfficherConsole();
    }

    public void AfficherRapportPersonnageStatFilms()
    {
        /*Code à remplacer pour utiliser une classe utilitaire*/
        int universId;
        bool valide;
        do
        {
            Console.WriteLine("Entrer la clé de l'univers.");

            valide = Int32.TryParse(Console.ReadLine(), out universId);

            if (valide == false)
            {
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
            }

        } while (valide == false);

        //Mettre ce genre de questions dans un utilitaire.
        //Il faut s'assurer que c'est seulement O ou N. 
        //Dans cet exemple, le cas Z sera considéré comme un N

        Console.WriteLine("Il faut utiliser la vue SQL ? [O/N]");
        bool utiliserVue;

        if (Console.ReadLine()?.ToUpper() == "O")
        {
            utiliserVue = true;
        }
        else
        {
            utiliserVue = false;
        }

        _personnageService.ObtenirRapportPersonnageStatFilms(universId, utiliserVue).AfficherConsole();
    }

    public void GenererDonneesBD()
    {
        /*Code à remplacer pour utiliser une classe utilitaire*/
        int quantite;
        bool valide;
        do
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Combien de personnages voulez-vous générer ?");
            Console.ForegroundColor = ConsoleColor.Gray;

            valide = Int32.TryParse(Console.ReadLine(), out quantite);

            if (valide == false)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
                Console.ForegroundColor = ConsoleColor.Gray;
            }

            if (quantite <= 0)
            {
                valide = false;

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("La quantité doit être plus grande que zéro.");
                Console.ForegroundColor = ConsoleColor.Gray;
            }

        } while (valide == false);
        /*Fin du code à remplacer*/

        _genererPersonnageBDService.Generer(quantite);
    }
}