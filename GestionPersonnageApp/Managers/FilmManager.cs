﻿namespace GestionPersonnageApp.Managers;

/// <summary>
/// Classe qui s'occupe de la coordination de modèle Film
/// </summary>
public class FilmManager : IFilmManager
{
    private readonly IFilmService _filmService;
    private readonly IGenererFilmEtDistributionBDService _genererFilmEtDistributionBDService;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="filmService">Service du modèle Film</param>
    /// <param name="genererFilmEtDistributionBDService">Service de génération de données dans la BD pour du modèle Film et Distribution</param>
    public FilmManager(IFilmService filmService, IGenererFilmEtDistributionBDService genererFilmEtDistributionBDService)
    {
        _filmService = filmService;
        _genererFilmEtDistributionBDService = genererFilmEtDistributionBDService;
    }

    public void AfficherListe()
    {
        //Utilisation directe de l'affichage
        _filmService.ObtenirListe().AfficherConsole();
    }

    public void AfficherParId()
    {

        /*Code à remplacer pour utiliser une classe utilitaire*/
        int filmId;
        bool valide;
        do
        {
            Console.WriteLine("Entrer la clé du film.");

            valide = Int32.TryParse(Console.ReadLine(), out filmId);

            if (valide == false)
            {
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
            }

        } while (valide == false);
        /*Fin du code à remplacer*/

        //Utilisation directe de l'affichage
        _filmService.ObtenirFilm(filmId).AfficherConsole();
    }

    public void AfficherFilmDetailParId()
    {
        /*Code à remplacer pour utiliser une classe utilitaire*/
        int filmId;
        bool valide;
        do
        {
            Console.WriteLine("Entrer la clé du film.");

            valide = Int32.TryParse(Console.ReadLine(), out filmId);

            if (valide == false)
            {
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
            }

        } while (valide == false);
        /*Fin du code à remplacer*/

        //Utilisation directe de l'affichage
        _filmService.ObtenirFilmDetail(filmId).AfficherConsoleFilmDetail();
    }
        
    public void GenererDonneesBD()
    {
        /*Code à remplacer pour utiliser une classe utilitaire*/
        int quantite;
        bool valide;
        do
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Combien de films voulez-vous générer ?");
            Console.ForegroundColor = ConsoleColor.Gray;

            valide = Int32.TryParse(Console.ReadLine(), out quantite);

            if (valide == false)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
                Console.ForegroundColor = ConsoleColor.Gray;
            }

            if (quantite <= 0)
            {
                valide = false;

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("La quantité doit être plus grande que zéro.");
                Console.ForegroundColor = ConsoleColor.Gray;
            }

        } while (valide == false);
        /*Fin du code à remplacer*/

        _genererFilmEtDistributionBDService.Generer(quantite);
    }
}