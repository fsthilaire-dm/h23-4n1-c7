﻿namespace GestionPersonnageApp.Managers;

/// <summary>
/// Interface qui s'occupe de la coordination du modèle Distribution
/// </summary>
public interface IDistributionManager
{
    /// <summary>
    /// Afficher toutes les distributions
    /// </summary>
    void AfficherListe();

    /// <summary>
    /// Afficher une distribution en fonction de ses clés primaires
    /// </summary>        
    void AfficherParIds();

    /// <summary>
    /// Supprimer une distribution en fonction de ses clés primaires
    /// </summary>
    void SupprimerParIds();

    /// <summary>
    /// Modifier l'acteur d'une distribution en fonction de ses clés primaires
    /// </summary>
    void ModifierActeurParIds();
}