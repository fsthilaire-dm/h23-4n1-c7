﻿namespace GestionPersonnageApp.Managers;

/// <summary>
/// Classe qui s'occupe de la coordination du modèle Distribution
/// </summary>
public class DistributionManager : IDistributionManager
{
    private readonly IDistributionService _distributionService;

    public DistributionManager(IDistributionService distributionService)
    {
        _distributionService = distributionService;
    }

    public void AfficherListe()
    {
        /*Code à remplacer pour utiliser une classe utilitaire*/
        //Mettre ce genre de questions dans un utilitaire.
        //Il faut s'assurer que c'est seulement O ou N. 
        //Dans cet exemple, le cas Z sera considéré comme un N

        Console.WriteLine("Il faut afficher le détail des clés étrangères ? [O/N]");
        bool afficherCleEtrangere;

        if (Console.ReadLine()?.ToUpper() == "O")
        {
            afficherCleEtrangere = true;
        }
        else
        {
            afficherCleEtrangere = false;
        }

        /*Fin du code à remplacer*/

        //Utilisation directe de l'affichage
        _distributionService.ObtenirListe(afficherCleEtrangere).AfficherConsole();
    }

    public void AfficherParIds()
    {

        /*Code à remplacer pour utiliser une classe utilitaire*/
        int personnageId;
        int filmId;

        bool valide;
        do
        {
            Console.WriteLine("Entrer la clé du personnage.");

            valide = Int32.TryParse(Console.ReadLine(), out personnageId);

            if (valide == false)
            {
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
            }

        } while (valide == false);

        
        do
        {
            Console.WriteLine("Entrer la clé du film.");

            valide = Int32.TryParse(Console.ReadLine(), out filmId);

            if (valide == false)
            {
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
            }

        } while (valide == false);
        /*Fin du code à remplacer*/

        /*Code à remplacer pour utiliser une classe utilitaire*/
        //Mettre ce genre de questions dans un utilitaire.
        //Il faut s'assurer que c'est seulement O ou N. 
        //Dans cet exemple, le cas Z sera considéré comme un N

        Console.WriteLine("Il faut afficher le détail des clés étrangères ? [O/N]");
        bool afficherCleEtrangere;

        if (Console.ReadLine()?.ToUpper() == "O")
        {
            afficherCleEtrangere = true;
        }
        else
        {
            afficherCleEtrangere = false;
        }

        /*Fin du code à remplacer*/

        //Utilisation directe de l'affichage
        _distributionService.ObtenirDistribution(personnageId, filmId, afficherCleEtrangere).AfficherConsole();
    }

    public void SupprimerParIds()
    {
        /*Code à remplacer pour utiliser une classe utilitaire*/
        int personnageId;
        int filmId;

        bool valide;
        do
        {
            Console.WriteLine("Entrer la clé du personnage.");

            valide = Int32.TryParse(Console.ReadLine(), out personnageId);

            if (valide == false)
            {
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
            }

        } while (valide == false);


        do
        {
            Console.WriteLine("Entrer la clé du film.");

            valide = Int32.TryParse(Console.ReadLine(), out filmId);

            if (valide == false)
            {
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
            }

        } while (valide == false);
        /*Fin du code à remplacer*/

        /*Code à remplacer pour utiliser une classe utilitaire*/
        //Mettre ce genre de questions dans un utilitaire.
        //Il faut s'assurer que c'est seulement O ou N. 
        //Dans cet exemple, le cas Z sera considéré comme un N

        Console.WriteLine("Il faut afficher le détail des clés étrangères ? [O/N]");
        bool afficherCleEtrangere;

        if (Console.ReadLine()?.ToUpper() == "O")
        {
            afficherCleEtrangere = true;
        }
        else
        {
            afficherCleEtrangere = false;
        }

        /*Fin du code à remplacer*/

        Distribution? distribution = _distributionService.ObtenirDistribution(personnageId, filmId, afficherCleEtrangere);

        bool supprimer = distribution.SupprimerConsole();

        if (supprimer == true)
        {
            //Il y a un !. ,car la validation du cas null se fait avec la méthode d'extension SupprimerConsole
            //Le !. indique au compilateur d'accepter l'avertissement
            _distributionService.SupprimerDistribution(distribution!.PersonnageId, distribution!.FilmId);
        }
    }

    public void ModifierActeurParIds()
    {
        /*Code à remplacer pour utiliser une classe utilitaire*/
        int personnageId;
        int filmId;

        bool valide;
        do
        {
            Console.WriteLine("Entrer la clé du personnage.");

            valide = Int32.TryParse(Console.ReadLine(), out personnageId);

            if (valide == false)
            {
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
            }

        } while (valide == false);


        do
        {
            Console.WriteLine("Entrer la clé du film.");

            valide = Int32.TryParse(Console.ReadLine(), out filmId);

            if (valide == false)
            {
                Console.WriteLine("Ce n'est pas un nombre. Essayez de nouveau.");
            }

        } while (valide == false);
        /*Fin du code à remplacer*/

        /*Code à remplacer pour utiliser une classe utilitaire*/
        //Mettre ce genre de questions dans un utilitaire.
        //Il faut s'assurer que c'est seulement O ou N. 
        //Dans cet exemple, le cas Z sera considéré comme un N

        Console.WriteLine("Il faut afficher le détail des clés étrangères ? [O/N]");
        bool afficherCleEtrangere;

        if (Console.ReadLine()?.ToUpper() == "O")
        {
            afficherCleEtrangere = true;
        }
        else
        {
            afficherCleEtrangere = false;
        }

        /*Fin du code à remplacer*/
                
        Distribution? distribution = _distributionService.ObtenirDistribution(personnageId, filmId, afficherCleEtrangere);

        distribution.AfficherConsole();

        //Vérifie si la distribution existe avant d'effectuer la modification
        if (distribution != null)
        {
            //La distribution existe. La modification peut débuter
            bool acteurValide;

            do
            {                
                Console.WriteLine("Le nouveau nom de l'acteur : ");

                string acteur = Console.ReadLine()!;

                acteurValide = _distributionService.ModifierActeur(distribution.PersonnageId, distribution.FilmId, acteur);

                if(acteurValide == false)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Erreur. Le nom de l'acteur n'est pas valide.");
                    Console.WriteLine("La longueur doit être entre 1 et 100 caractères.");
                    Console.ForegroundColor = ConsoleColor.Gray;
                }
            } while (acteurValide == false);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Modification effectuée.");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}