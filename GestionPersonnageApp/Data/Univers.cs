﻿using System;
using System.Collections.Generic;

namespace GestionPersonnageApp.Data;

public partial class Univers
{
    public int UniversId { get; set; }

    public string Nom { get; set; } = null!;

    public short AnneeCreation { get; set; }

    public string SiteWeb { get; set; } = null!;

    public string Proprietaire { get; set; } = null!;

    public virtual ICollection<Personnage> Personnage { get; } = new List<Personnage>();
}
