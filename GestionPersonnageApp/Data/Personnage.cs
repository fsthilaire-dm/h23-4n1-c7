﻿using System;
using System.Collections.Generic;

namespace GestionPersonnageApp.Data;

public partial class Personnage
{
    public int PersonnageId { get; set; }

    public string Nom { get; set; } = null!;

    public string? IdentiteReelle { get; set; }

    public DateTime? DateNaissance { get; set; }

    public bool EstVilain { get; set; }

    public int UniversId { get; set; }

    public virtual ICollection<Distribution> Distribution { get; } = new List<Distribution>();

    public virtual Univers Univers { get; set; } = null!;
}
