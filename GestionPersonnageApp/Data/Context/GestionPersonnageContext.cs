﻿using System;
using System.Collections.Generic;
using GestionPersonnageApp.Data;
using Microsoft.EntityFrameworkCore;

namespace GestionPersonnageApp.Data.Context;

public partial class GestionPersonnageContext : DbContext
{
    public GestionPersonnageContext(DbContextOptions<GestionPersonnageContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Distribution> Distribution { get; set; }

    public virtual DbSet<Film> Film { get; set; }

    public virtual DbSet<Personnage> Personnage { get; set; }

    public virtual DbSet<Univers> Univers { get; set; }

    public virtual DbSet<VRapportPersonnageStatFilms> VRapportPersonnageStatFilms { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Distribution>(entity =>
        {
            entity.HasKey(e => new { e.PersonnageId, e.FilmId });

            entity.Property(e => e.Acteur)
                .HasMaxLength(100)
                .IsUnicode(false);

            entity.HasOne(d => d.Film).WithMany(p => p.Distribution)
                .HasForeignKey(d => d.FilmId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Distribution_FilmId");

            entity.HasOne(d => d.Personnage).WithMany(p => p.Distribution)
                .HasForeignKey(d => d.PersonnageId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Distribution_PersonnageId");
        });

        modelBuilder.Entity<Film>(entity =>
        {
            entity.Property(e => e.Budget).HasColumnType("decimal(10, 2)");
            entity.Property(e => e.DateSortie).HasColumnType("date");
            entity.Property(e => e.Titre)
                .HasMaxLength(100)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Personnage>(entity =>
        {
            entity.Property(e => e.DateNaissance).HasColumnType("datetime");
            entity.Property(e => e.IdentiteReelle)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Nom)
                .HasMaxLength(100)
                .IsUnicode(false);

            entity.HasOne(d => d.Univers).WithMany(p => p.Personnage)
                .HasForeignKey(d => d.UniversId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SuperHeros_UniversId");
        });

        modelBuilder.Entity<Univers>(entity =>
        {
            entity.Property(e => e.Nom)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Proprietaire)
                .HasMaxLength(250)
                .IsUnicode(false);
            entity.Property(e => e.SiteWeb)
                .HasMaxLength(2000)
                .IsUnicode(false);
        });

        modelBuilder.Entity<VRapportPersonnageStatFilms>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("vRapportPersonnageStatFilms");

            entity.Property(e => e.BudgetMoyen).HasColumnType("decimal(38, 6)");
            entity.Property(e => e.PersonnageNom)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.UniversNom)
                .HasMaxLength(100)
                .IsUnicode(false);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
