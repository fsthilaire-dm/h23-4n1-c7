﻿using System;
using System.Collections.Generic;

namespace GestionPersonnageApp.Data;

public partial class Film
{
    public int FilmId { get; set; }

    public string Titre { get; set; } = null!;

    public DateTime DateSortie { get; set; }

    public byte Etoile { get; set; }

    public short Duree { get; set; }

    public decimal Budget { get; set; }

    public virtual ICollection<Distribution> Distribution { get; } = new List<Distribution>();
}
