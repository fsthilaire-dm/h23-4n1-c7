﻿using System;
using System.Collections.Generic;

namespace GestionPersonnageApp.Data;

public partial class Distribution
{
    public int PersonnageId { get; set; }

    public int FilmId { get; set; }

    public string Acteur { get; set; } = null!;

    public virtual Film Film { get; set; } = null!;

    public virtual Personnage Personnage { get; set; } = null!;
}
