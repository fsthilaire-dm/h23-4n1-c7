﻿namespace GestionPersonnageApp.Outils;

/// <summary>
/// Interface qui représente l'outil de génération d'un univers
/// </summary>
public interface IUniversGenerateur
{
    /// <summary>
    /// Génère un univers
    /// </summary>
    /// <returns>Un objet univers généré</returns>
    public Univers Generer();
}
