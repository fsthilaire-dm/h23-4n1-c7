﻿using Bogus;
using GestionPersonnageApp.Data;

namespace GestionPersonnageApp.Outils;

/// <summary>
/// Classe qui permet de générer des objets distributions dans un objet film
/// </summary>
public class DistributionGenerateur : Faker<Distribution>, IDistributionGenerateur
{
    private List<int> _lstPersonnageId = new List<int>();

    public DistributionGenerateur()
    {
        RuleFor(i => i.Acteur, GenActeur);
        RuleFor(i => i.PersonnageId, GenPersonnageId);
    }

    public void Generer(Film film, int min, int max)
    {
        Faker f = new Faker();

        //Détermine le nombre de distributions à générer
        int nbDistribution = f.Random.Number(min, max);

        //Le code ci-dessous permet de s'assurer que le même PersonnageId n'est pas pigé plusieurs fois.        
        do
        {
            Distribution distribution = base.Generate();

            //Vérifie s'il y a une distribution pour ce film qui a déjà ce personnage
            if(film.Distribution.Count(d => d.PersonnageId == distribution.PersonnageId) == 0)
            {
                //Le personnage n'a jamais été utilisé pour une distribution de ce film
                //La distribution est ajoutée à la base de données, en assignant le FilmId du film en cours
                film.Distribution.Add(distribution);

                nbDistribution--;
            }

        } while (nbDistribution > 0);
    }

    private string GenActeur(Faker f)
    {
        //VARCHAR(100)
        return f.Name.FullName().Tronquer(100)!;
    }

    private int GenPersonnageId(Faker f)
    {
        //Doit choisir un personnage dans la liste
        return f.PickRandom(_lstPersonnageId);
    }

    public void AssignerListePersonnageId(List<int> lstPersonnageId)
    {
        _lstPersonnageId.Clear();
        _lstPersonnageId.AddRange(lstPersonnageId);
    }
}
