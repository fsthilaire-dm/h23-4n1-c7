﻿namespace GestionPersonnageApp.Outils;

/// <summary>
/// Interface qui représente l'outil de génération d'un film
/// </summary>
public interface IDistributionGenerateur
{
    /// <summary>
    /// Génère des distributions pour un film
    /// </summary>
    /// <param name="film">Le film qui doit avoir des distributions</param>
    /// <param name="min">La quantité minimale de distributions à générer</param>
    /// <param name="min">La quantité maximale de distribution à générer</param>
    /// <returns>Des distributions générées et assignées au film</returns>
    void Generer(Film film, int min, int max);

    /// <summary>
    /// Assigner la liste des clés disponibles pour des personnages
    /// </summary>
    /// <param name="lstPersonnageId">Liste des clés personnage id</param>
    void AssignerListePersonnageId(List<int> lstPersonnageId);
}