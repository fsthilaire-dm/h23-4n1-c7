﻿namespace GestionPersonnageApp.Outils;

/// <summary>
/// Classe qui permet de générer un objet personnage
/// </summary>
public class PersonnageGenerateur : Faker<Personnage>, IPersonnageGenerateur
{
    private List<int> _lstUniversId = new List<int>();
    private string[] _prefixePersonnage = 
        new string[] { "Super ", "Captain ", "Master ", "Evil ", "Max ", "Doctor ", "Prof ", "The " };

    public PersonnageGenerateur()
    {
        RuleFor(i => i.Nom, GenNom);
        RuleFor(i => i.IdentiteReelle, GenIdentiteReelle);
        RuleFor(i => i.DateNaissance, GenDateNaissance);
        RuleFor(i => i.EstVilain, GenEstVilain);
        RuleFor(i => i.UniversId, GenUniversId);
    }

    public Personnage Generer()
    {
        return base.Generate();
    }

    public void AssignerListeUniversId(List<int> lstUniversId)
    {
        _lstUniversId.Clear();
        _lstUniversId.AddRange(lstUniversId);
    }

    /// <summary>
    /// Générateur de la propriété Nom
    /// </summary>
    /// <param name="f">Faker</param>
    /// <returns>Le nom généré</returns>
    private string GenNom(Faker f)
    {
        //Il y a autant de change d'avoir un préfixe ou aucun préfixe.        
        string prefixe = f.PickRandom(new string[] { f.PickRandom(_prefixePersonnage), string.Empty });

        //Vérifie s'il y a un préfixe
        if (prefixe != string.Empty)
        {
            //Il y a un préfixe, le nom est un mot pour générer un nom de super-héros
            //Ajout du ! pour enlever l'avertissement
            //VARCHAR(100)
            return $"{prefixe}{f.Random.Words()}".Tronquer(100)!;
        }
        else
        {
            //Il n'y a pas de préfixe, le nom est un nom réel
            //Il y a 25% de chance d'avoir uniquement un prénom, 35% d'avoir un nom de famille et 40% d'avoir un nom complet.
            string[] choix = new string[3];
            choix[0] = f.Name.FirstName();
            choix[1] = f.Name.LastName();
            choix[2] = $"{choix[0]} {choix[1]}";

            //Ajout du ! pour enlever l'avertissement
            //VARCHAR(100)
            return f.Random.WeightedRandom(choix, new float[] {0.25f, 0.35f, 0.4f}).Tronquer(100)!;
        }        
    }

    /// <summary>
    /// Générateur de l'identité réelle générée
    /// </summary>
    /// <param name="f">Faker</param>
    /// <param name="personnage">Objet personnage en cours de génération</param>
    /// <returns></returns>
    private string? GenIdentiteReelle(Faker f, Personnage personnage)
    {
        //L'identité réelle peut être le nom du personnage ou un nouveau nom.
        string?[] choix = new string[2];

        choix[0] = personnage.Nom;

        //Il est possible d'avoir une identité réelle null. La probabilité que l'identité soit null est de 40%.        
        choix[1] = f.Name.FullName().OrNull(f, 0.4f);

        //La probabilité que l'identité réelle soit identique au nom est de 5%
        //Doit tronquer pour VARCHAR(100)
        return f.Random.WeightedRandom(choix, new float[] { 0.05f, 0.95f }).Tronquer(100);
    }

    /// <summary>
    /// Générateur de la propriété DateNaissance
    /// </summary>
    /// <param name="f">Faker</param>
    /// <returns>La date de naissance générée</returns>
    private DateTime? GenDateNaissance(Faker f) 
    {
        //Sélectionne une date entre 1753-01-01 et 2299-12-31.
        //La plus petite date de la BD est 1753-01-01.
        //Il est possible d'avoir des personnages dans le futur.
        //Il est possible d'avoir une date null. LA probabilité que la date soit null est de 60%.
        return f.Date.Between(new DateTime(1753, 1, 1), new DateTime(2299, 12, 31)).Date.OrNull(f, 0.6f);
    }

    /// <summary>
    /// Générateur de la propriété EstVilain
    /// </summary>
    /// <param name="f">Faker</param>
    /// <returns>Vrai ou Faux</returns>
    private bool GenEstVilain(Faker f)
    {
        //Effectue un random avec une probabilité. Il y a 30% de chance que le personnage soit vilain.
        return f.Random.Bool(0.3f);
    }

    /// <summary>
    /// Générateur de la propriété UniversId
    /// </summary>
    /// <param name="f">Faker</param>
    /// <returns>Un univers id</returns>
    private int GenUniversId(Faker f)
    {
        //Sélectionne un univers id dans la liste disponible.
        return f.PickRandom(_lstUniversId);
    }
}
