﻿namespace GestionPersonnageApp.Outils;

/// <summary>
/// Classe qui permet de générer un objet univers
/// </summary>
public class UniversGenerateur : Faker<Univers>, IUniversGenerateur
{    
    public UniversGenerateur()
    {
        RuleFor(i => i.Nom, GenNom);
        RuleFor(i => i.AnneeCreation, GenAnneeCreation);
        RuleFor(i => i.SiteWeb, GenSiteWeb);
        RuleFor(i => i.Proprietaire, GenProprietaire);
    }

    public Univers Generer()
    {
        return base.Generate();
    }

    /// <summary>
    /// Générateur de la propriété Nom
    /// </summary>
    /// <param name="f">Faker</param>
    /// <returns>Le nom généré</returns>
    private string GenNom(Faker f)
    {
        //Trunque la chaine à 100, car dans la base de données, c'est VARCHAR(100)
        string nom = f.Random.Words().Tronquer(100)!;

        //Sélectionne la première lettre et la transforme en majuscule et ajoute le reste
        return nom.Substring(0, 1).ToUpper() + nom.Substring(1);
    }

    /// <summary>
    /// Générateur de la propriété AnneeCreation
    /// </summary>
    /// <param name="f">Faker</param>
    /// <returns>L'année de création générée</returns>
    private short GenAnneeCreation(Faker f)
    {
        //Année minimale est 1910 et l'année maximale est cette année
        return f.Random.Short(1910, (short)DateTime.Now.Year);
    }

    /// <summary>
    /// Générateur de la propriété SiteWeb
    /// </summary>
    /// <param name="f">Faker</param>
    /// <param name="univers">Objet univers en cours de génération</param>
    /// <returns>Le site web généré</returns>
    private string GenSiteWeb(Faker f, Univers univers)
    {
        //Transforme le nom de l'univers en minuscule.
        //Remplace les espaces par un - ou rien. 
        //La fonction PickRandom permet de choisir au hasard dans une liste
        string domain = univers.Nom.ToLower().Replace(" ", f.PickRandom("-", ""));

        //Effectue un random avec probabilité
        //Il y a 70% e chance d'obtenir le sous domain "https://www.", 10% "http://www", 25% "https://" et 5% "http://"
        //Il est possible de mettre le nombre de choix désiré. Il faut que le tableau de probabilité soit de longueur identique et que la somme des probabilités soit 1.
        string subdomain = f.Random.WeightedRandom(new string[] { "https://www.", "http://www.", "https://", "http://" },
                                                   new float[] { 0.6f, 0.1f, 0.25f, 0.05f });

        //Ajoute à la fin un suffixe de domaine
        string urlComplet = subdomain + domain + "." + f.Internet.DomainSuffix();

        //Tunquer pour VARCHAR(2000)
        return urlComplet.Tronquer(2000)!;
    }

    /// <summary>
    /// Générateur de la propriété Propriétaire
    /// </summary>
    /// <param name="f">Faker</param>
    /// <returns>Le propriétaire généré</returns>
    private string GenProprietaire(Faker f)
    {
        //Tronquer pour VARCHAR(250)
        return f.Company.CompanyName().Tronquer(250)!;
    }
}
