﻿namespace GestionPersonnageApp.Outils;

/// <summary>
/// Classe qui permet de générer un objet film
/// </summary>
public class FilmGenerateur : Faker<Film>, IFilmGenerateur
{
    public FilmGenerateur()
    {
        RuleFor(i => i.Titre, GenTitre);
        RuleFor(i => i.DateSortie, GenDateSortie);
        RuleFor(i => i.Etoile, GenEtoile);
        RuleFor(i => i.Duree, GenDuree);
        RuleFor(i => i.Budget, GenBudget);        
        //Il n'y a pas de RuleFor pour la collection de la distribution.
        //Le RuleFor fait une assignation. Dans le cas de Entity, il faut faire un ajout à la collection existante.
    }

    public Film Generer()
    {
        return base.Generate();
    }

    private string GenTitre(Faker f)
    {
        //Le titre est un série de mots
        //VARCHAR(100)
        return f.Random.Words().Tronquer(100)!;
    }

    private DateTime GenDateSortie(Faker f)
    {
        //Permet de choisir une date entre 1960-01-01 et une date aléatoire dans le futur d'ici l'an prochain.
        return f.Date.Between(new DateTime(1960, 1, 1), f.Date.Future(1)).Date;
    }

    private byte GenEtoile(Faker f)
    {
        //Contrainte CK_Film_Etoile CHECK de 1 à 10
        return f.Random.Byte(1, 10);
    }

    private short GenDuree(Faker f)
    {
        //La durée est entre 10 et 362 minutes
        return f.Random.Short(10, 362);
    }

    private decimal GenBudget(Faker f)
    {
        //Génère un montant entre 0.00 et 1000.00
        //Le budget est en million
        return f.Finance.Amount();
    }
}
