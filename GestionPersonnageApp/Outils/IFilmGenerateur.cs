﻿namespace GestionPersonnageApp.Outils;

/// <summary>
/// Interface qui représente l'outil de génération d'un film
/// </summary>
public interface IFilmGenerateur
{
    /// <summary>
    /// Génère un film
    /// </summary>
    /// <returns>Un objet film généré</returns>
    Film Generer();
}