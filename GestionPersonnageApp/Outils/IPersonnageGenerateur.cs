﻿namespace GestionPersonnageApp.Outils;

/// <summary>
/// Interface qui représente l'outil de génération d'un personnage
/// </summary>
public interface IPersonnageGenerateur
{
    /// <summary>
    /// Génère un personnage
    /// </summary>
    /// <returns>Un objet personnage généré</returns>
    public Personnage Generer();

    /// <summary>
    /// Assigner la liste des clés disponibles pour des univers
    /// </summary>
    /// <param name="lstUniversId">Liste des clés univers id</param>
    public void AssignerListeUniversId(List<int> lstUniversId);
}